FROM python:3.7
ENV PYTHONUNBUFFERED 1
RUN mkdir -p /code
WORKDIR /code
COPY requirements* /code/
RUN pip install -U pip
RUN pip install -r requirements-dev.txt
